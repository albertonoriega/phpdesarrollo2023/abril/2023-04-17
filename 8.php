<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $nombre="juan";
        $poblacion=null;
        $apellidos = "";
        //La función empty comprueba que la variable esté vacia
        var_dump(empty($nombre)); //false (la variable no está vacia) 
        var_dump(empty($edad)); //true (la variable no existe)
        var_dump(empty($poblacion)); // true (la variable está a null)
        var_dump(empty($apellidos)); // true (la variable está vacia)
    ?>
</body>
</html>