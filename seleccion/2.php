<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    $dia=2;
    $diaDeLaSemana= "";

    switch ($dia) {
        case 1:
            $diaDeLaSemana = 'Lunes';
            break;
        case 2:
            $diaDeLaSemana = 'Martes';
            break;
        case 3:
            $diaDeLaSemana = 'Miercoles';
            break;
        case 4:
            $diaDeLaSemana = 'Jueves';
            break;
        case 5:
            $diaDeLaSemana = 'Viernes';
            break;
        case 6:
            $diaDeLaSemana = 'Sabado';
            break;
        case 7:
            $diaDeLaSemana = 'Domingo';
            break;
        default:
            $diaDeLaSemana = "Valor introducido no correcto";
            break;
    }
    ?>

    <div>Dia: <?= $dia?></div>
    <div>Dia de la semana: <?= $diaDeLaSemana?></div>
</body>
</html>