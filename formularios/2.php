<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li{
            list-style: none;
            padding: 10px;
            border: 2px solid red;
            margin: 2px;
            width: 150px;
        }
        li:hover{
            background-color: #ccc;
        }
        span{
            font-weight: bold;
            color: blue;
        }
    </style>
</head>
<body>
    
<?php
//Inicializamos las variables
$nombre="";
$edad=0;
$poblacion="";

//Comprobar que el usuario ha pulsado enviar
// isset comprueba que la variable exista
if (isset($_GET["enviar"])) {
    
$nombre=$_GET["nombre"];
$edad=$_GET["edad"];  
$poblacion=$_GET["poblacion"];
}



?>

<ul>
        <li><span> Nombre: </span> <?= $nombre?></li>
        <li><span>Edad: </span><?= $edad?></li>
        <li><span>Población: </span><?= $poblacion ?> </li>
    </ul>


</body>
</html>

