<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li{
            list-style: none;
            padding: 10px;
            border: 2px solid red;
            margin: 2px;
            width: 150px;
        }
        li:hover{
            background-color: #ccc;
        }
        span{
            font-weight: bold;
            color: blue;
        }
    </style>
</head>
<body>
    
<?php


//Inicializamos las variables
$poblacion="";
$provincia = "";
$calle = "";

//Comprobar que el usuario ha pulsado enviar

if (isset($_GET["enviar"])) {

    if(!empty($_GET["poblacion"])){
        $poblacion=$_GET["poblacion"];
    }
    if(!empty($_GET["provincia"])){
        $provincia=$_GET["provincia"];  
    }
    if(!empty($_GET["calle"])){
        $calle=$_GET["calle"];
    }  

/*
SI SE DEJAN LOS TRES CAMPOS VACIOS:
Poblacion = Santander
Provincia = Cantabria
Calle = Calle Vargas
*/

    if(empty($_GET["poblacion"]) && empty($_GET["provincia"]) && empty($_GET["calle"] )){
        $poblacion = "Santander";
        $provincia = "Cantabria";
        $calle = "Calle Vargas";
    }

}

?>

<ul>
    <li><span> Población: </span> <?= $poblacion?></li>
    <li><span>Provincia: </span><?= $provincia?></li>
    <li><span>Calle: </span><?= $calle ?> </li>
</ul>
</body>
</html>